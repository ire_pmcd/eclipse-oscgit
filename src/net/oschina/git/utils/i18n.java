package net.oschina.git.utils;

import org.eclipse.osgi.util.NLS;

public class i18n extends NLS{
	private static final String BUNDLE_NAME = "net.oschina.git.utils.i18n"; //$NON-NLS-1$
	public static String Email;
	public static String Password;
	public static String Host;
	public static String Auth_type;
	public static String Save_password;
	public static String login;
	public static String Search;
	public static String Test;
	public static String parentdir;
	public static String dirName;
	public static String cloneRep;
	public static String giturl;
	public static String Login;
	public static String Help;
	public static String Cancel;
	public static String Clone;
	public static String loginError;
	public static String Unauthorized;
	public static String Private;
	public static String RemoteName;
	public static String NewRepName;
	public static String Description;
	public static String passwordWrong;
	static {
		initializeMessages(BUNDLE_NAME, i18n.class);
	}
}
